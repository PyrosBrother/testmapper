#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QSqlQuery>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    if (setupModel()) {
        ui->labelNom->setEnabled(true);
        ui->lineEditNom->setEnabled(true);
        ui->checkBoxRemise->setEnabled(true);
        ui->labelListe->setEnabled(true);
        ui->tableViewEnregistrements->setEnabled(true);
        ui->pushButtonAjouter->setEnabled(true);
        ui->pushButtonEditer->setEnabled(true);
        ui->pushButtonEnregistrer->setEnabled(true);
        ui->pushButtonPrecedent->setEnabled(true);
        ui->pushButtonSuivant->setEnabled(true);
        ui->pushButtonSupprimer->setEnabled(true);

        ui->tableViewEnregistrements->setModel(model);

        mapper = new QDataWidgetMapper(this);
        mapper->setModel(model);
        mapper->addMapping(ui->lineEditNom, model->fieldIndex("nommodregl"));
        mapper->addMapping(ui->checkBoxRemise, model->fieldIndex("remise"));

        mapper->toFirst();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::setupModel()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("SeriePratikMac");
    db.setUserName("essai");
    db.setPassword("testbdd");

    if (!db.open()) {
        QMessageBox::critical(0, tr("Erreur à l'ouverture de la base de données"),
                              tr("Vérifiez les paramètres de connexion.\n"
                                 "Cet example nécessite une connexion à PostgreSql. "
                                 "Vérifiez que le support de PostgreSql est bien actif dans Qt. "
                                 "Si besoin ajoutez le en lisant la documentation spécifique à QSql."), QMessageBox::Cancel);
        return false;
    }

    QStringList listeTables=db.tables();
    if (!listeTables.contains("modregl", Qt::CaseInsensitive)) {
        QSqlQuery query;
        query.exec("CREATE TABLE modregl ( idmodregl serial NOT NULL, nommodregl character varying(20), remise smallint, CONSTRAINT ""IDMODREGL"" PRIMARY KEY (idmodregl) )");
        query.exec("truncate modregl");
        query.exec("insert into modregl values (1, 'Chèque', 1)");
        query.exec("insert into modregl values (2, 'Virement', 0)");
        query.exec("insert into modregl values (3, 'Carte bleue', 0)");
        query.exec("insert into modregl values (4, 'Espèces', 1)");
    }

    model = new QSqlTableModel(this);
    model->setTable("modregl");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);

    model->select();
    return true;
}

void MainWindow::on_pushButtonQuitter_clicked()
{
    close();
}

void MainWindow::on_pushButtonAjouter_clicked()
{
model->insertRows (model->rowCount(), 1);
}

void MainWindow::on_pushButtonsupprimer_clicked()
{
    QModelIndex  indexSelectionne=ui->tableViewEnregistrements->currentIndex();
    if (indexSelectionne.isValid()) {
        model->removeRows(indexSelectionne.row(), 1);
    }
}

void MainWindow::on_pushButtonPrecedent_clicked()
{
    mapper->toPrevious();
}

void MainWindow::on_pushButtonSuivant_clicked()
{
    mapper->toNext();
}

void MainWindow::on_pushButtonEnregistrer_clicked()
{
    mapper->submit();
}

void MainWindow::on_pushButtonEditer_clicked()
{
    QModelIndex  indexSelectionne=ui->tableViewEnregistrements->currentIndex();
    if (indexSelectionne.isValid()) {
        mapper->setCurrentIndex(indexSelectionne.row());
    }
}
