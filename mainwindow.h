#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlTableModel>
#include <QDataWidgetMapper>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButtonQuitter_clicked();
    void on_pushButtonAjouter_clicked();
    void on_pushButtonsupprimer_clicked();
    void on_pushButtonPrecedent_clicked();
    void on_pushButtonSuivant_clicked();
    void on_pushButtonEnregistrer_clicked();

    void on_pushButtonEditer_clicked();

protected:
    bool setupModel();

private:
    Ui::MainWindow *ui;
    QSqlTableModel *model;
    QDataWidgetMapper *mapper;
};

#endif // MAINWINDOW_H
